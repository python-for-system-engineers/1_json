#!/usr/bin/env python3

import json
import random
import string


def randomString(stringLength=5):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))


def create_inventory():
    """ Create a dict with a hardware inventory, key is the hostname """
    inventory = {}
    hwtypes_server = ["ibm", "hp", "supermicro", "dell"]
    hwtypes_net = ["juniper", "cisco", "huawei"]    
    usage = ["router", "switch", "server", "storage"]
    os_server = ["centos", "archlinux", "ubuntu", "debian", "redhat"]
    os_net = ["JunOS", "Cisco 123"]
    virtualisation = [True, False]
    status = ["online", "offline", "old and shitty", "soooo legacy", "why???", "really nice hardware", "iops wonder", "my little server"]
    for i in range (0,100):    
        u = random.choice(usage)
        if ("router" in u) or ("switch" in u):
            hostname = "net_" + randomString()
            inventory[hostname] = {}
            inventory[hostname]["hardware"] = random.choice(hwtypes_net)
            inventory[hostname]["os"] = random.choice(os_net)
            inventory[hostname]["usage"] = u
            inventory[hostname]["status"] = random.choice(status)
        if ("server" in u) or ("storage" in u):
            hostname = "srv_" + randomString()
            inventory[hostname] = {}
            inventory[hostname]["hardware"] = random.choice(hwtypes_server)
            inventory[hostname]["os"] = random.choice(os_server)
            inventory[hostname]["usage"] = u
            inventory[hostname]["status"] = random.choice(status)
            inventory[hostname]["virtualisation"] = random.choice(virtualisation)
    return inventory			


def write_json_to_file(mydict):
    """ write a pretty json to file """
    with open ('inventory.json', 'w') as f:
        json.dump(mydict,f,indent=4)


def main():
    inventory = create_inventory()
    write_json_to_file(inventory)
	

if __name__== "__main__":
    main()
