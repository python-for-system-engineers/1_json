#!/usr/bin/env python3

import json


INVENTORY="inventory.json"

def get_inventory(inventory=INVENTORY):
    """ load a JSON formatted inventory file. Default inventory.json """
    with open(inventory) as inventory:
        inventory=json.load(inventory)
        return inventory


def filter_devices(inventory, devices="all"):
    """ filter devices. Default: all; accepts network, server """
    if "network" in devices:
        networkinventory = {}
        for deviceName, deviceAttrib in inventory.items():
            if ("router" in deviceAttrib["usage"]) or ("switch" in deviceAttrib["usage"]):
                networkinventory[deviceName] = deviceAttrib
        return networkinventory
    elif "server" in devices:
        serverinventory = {}
        for deviceName, deviceAttrib in inventory.items():
            if ("server" in deviceAttrib["usage"]) or ("storage" in deviceAttrib["usage"]):
                serverinventory[deviceName] = deviceAttrib
        return serverinventory
    else:
        return inventory


def filter_archlinux(inventory):
    """  rebuild inventory with archlinux then 2 statuses s1, and s2 """
    os = "archlinux"
    s1 = "really nice hardware"
    s2 = "iops wonder"
    archlinuxinventory = {}
    for deviceName, deviceAttrib in inventory.items():
        if "archlinux" in deviceAttrib["os"]:
            if (s1 in deviceAttrib["status"]) or (s2 in deviceAttrib["status"]):
                archlinuxinventory[deviceName] = deviceAttrib
    return archlinuxinventory


def filter_shiny(inventory):
    """  rebuild inventory with everything except 2 statuses: s1, and s2 """
    s1 = "old and shitty"
    s2 = "so legacy"
    shinyinventory = {}
    for deviceName, deviceAttrib in inventory.items():
        if not ((s1 in deviceAttrib["status"]) or (s2 in deviceAttrib["status"])):
            shinyinventory[deviceName] = deviceAttrib
    return shinyinventory


def get_inventory_statuses(inventory):
    """  """
    allStatuses = []
    uniqueStatuses = []
    for deviceName, deviceAttrib in inventory.items():
        allStatuses.append(deviceAttrib["status"])
    uniqueStatuses = list(set(allStatuses))
    return allStatuses, uniqueStatuses


def get_operating_systems(inventory):
    """  """
    allOperatingSystems = []
    uniqueOperatingSystems = []
    for deviceName, deviceAttrib in inventory.items():
        allOperatingSystems.append(deviceAttrib["os"])
    uniqueOperatingSystems = list(set(allOperatingSystems))
    return allOperatingSystems, uniqueOperatingSystems


def count_device_statuses(inventory):
    """  """
    # % of servers per status
    allStatuses, uniqueStatuses = get_inventory_statuses(inventory)
 
    deviceStatuses = {}
    for i in uniqueStatuses:
        deviceStatuses[i] = allStatuses.count(i)
    return deviceStatuses


def count_device_operating_systems(inventory):
    """  """
    # % of servers per status
    allOperatingSystems, uniqueOperatingSystems = get_operating_systems(inventory)
 
    operatingSystems = {}
    for i in uniqueOperatingSystems:
        operatingSystems[i] = allOperatingSystems.count(i)
    return operatingSystems


def get_device_operating_systems_stats(inventory):
    """  """
    # % of servers per status
    allOperatingSystems, uniqueOperatingSystems = get_operating_systems(inventory)
 
    operatingSystemsStats = {}
    for i in uniqueOperatingSystems:
        operatingSystemsStats[i] = "{0:.0%}".format(allOperatingSystems.count(i)/len(get_inventory()))
    return operatingSystemsStats


def write_json_to_file(inventory, output):
    """ write a pretty json to file """
    with open (output, 'w') as f:
        json.dump(inventory, f, indent=4)


def main():
    """  """

    print("Total devices       : " + str(len(get_inventory())))

    print("Network devices     : " + str(len(filter_devices(get_inventory(), devices="network"))))
    print("Network devices (%) : " + "{0:.0%}".format(len(filter_devices(get_inventory(), devices="network"))/len(get_inventory())))

    print("Server devices      : " + str(len(filter_devices(get_inventory(), devices="server"))))
    print("Server devices (%)  : " + "{0:.0%}".format(len(filter_devices(get_inventory(), devices="server"))/len(get_inventory())))

    print(count_device_operating_systems(get_inventory()))

    print(get_device_operating_systems_stats(get_inventory()))

    write_json_to_file(filter_archlinux(get_inventory()), "archlinux.json")

    write_json_to_file(filter_shiny(get_inventory()), "shiny.json")

    write_json_to_file(count_device_statuses(get_inventory()), "devicestatuscounts.json")

    write_json_to_file(count_device_operating_systems(get_inventory()), "operatingsystemcounts.json")

    write_json_to_file(get_device_operating_systems_stats(get_inventory()), "operatingsystemstats.json")


if __name__ == '__main__':
    main()
