# 1 JSON

Today you will learn to parse and process JSON

You will receive a JSON file that is an inventory of all your server and network devices

1. Execute createjson.py, it will save a file called inventory.json. You may also use the code in createjson.py as you like.
1. Write a python script that loads this JSON into a dicttionary and output this dictionary to the console.
1. Improve your python script so that you can create a JSON file that only contains archlinux server with the status "really nice hardware" or "iops wonder".
1. Decommissioning old servers is the best part of the job anyway. Give me a JSON file containing all servers and devices that do not have the statuss "old and shitty"and "so legacy".
1. Business Intelligence
    * What percentage of servers are in what status? What percentage of network devices? You may ignore the individual operating systems and can aggregate server/storage devices and router/switch devices
    * How many server/storage devices and router/switch devices are there?
    * What is the distribution of operating systems in percentages?
1. Finally, examine your code again. Where did you duplicate code? Where would you have been able to build functions for redundant code so that you ca reuse it? What else do you notice?

If you still have energy, make your code prettier